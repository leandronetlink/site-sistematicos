// Update with your config settings.
module.exports = {
  client: 'postgresql',
  connection: {
    database: 'sistematicos_db',
    user:     'postgres',
    password: '12345678',
    port: "5432",
    host:"localhost"
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    tableName: 'knex_migrations'
  }
};
