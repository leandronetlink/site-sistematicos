module.exports = app => {
    /*Importando as funções de validação do script 'validation'*/ 
    const { existsOrError, notExistsOrError, equalsOrError } = app.api.validation

    const save = async (req, res) => {
        /*Acessando todos os parametros da requisição e atribuindo no objeto 'evento'*/
        const UserAssociacao = { ...req.body }
        if(req.params.id) 
        UserAssociacao.id_user_has_associacao = req.params.id

        try {
            existsOrError(UserAssociacao.user_id_user, 'Admin não especificado')
            existsOrError(UserAssociacao.associacao_id_associacao, ' Associacao não informado')
            const userFromDB = await app.db('user_has_associacao')
                .where({ id_user_has_associacao: UserAssociacao.id_user_has_associacao }).first()
            if(!UserAssociacao.id_user_has_eventos) {
                notExistsOrError(userFromDB, 'Evento já cadastrado')
            }
        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(UserAssociacao.id_user_has_associacao) {
            app.db('user_has_associacao')
                .update(UserAssociacao)
                .where({ id_user_has_associacao: UserAssociacao.id_user_has_associacao })
                //.whereNull('deletedAt')
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('user_has_associacao')
                .insert(UserAssociacao)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        }
        
    }

    const get =  (req, res) => {
        app.db('user_has_associacao')
            .select('id_user_has_associacao', 'user_id_user', 'associacao_id_associacao')
            //.whereNull('deletedAt')
            .then(associacao => res.json(associacao))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('user_has_associacao')
            .select('id_user_has_eventos', 'user_id_user', 'associacao_id_associacao')
            .where({ id_user_has_associacao: req.params.id })
            //.whereNull('deletedAt')
            .first()
            .then(associacao => res.json(associacao))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        try {
            const rowsUpdated = await app.db('user_has_associacao')
                .where({ id_user_has_associacao: req.params.id }).del()
            existsOrError(rowsUpdated, 'Relacionamento não foi encontrado.')

            res.status(204).send()
        } catch(msg) {
            res.status(400).send(msg)
        }
    }

    return { save, get, getById, remove }
}