const bcrypt = require('bcrypt-nodejs')

module.exports = app => {
    const { existsOrError, notExistsOrError, equalsOrError } = app.api.validation

    const encryptPassword = password => {
        const salt = bcrypt.genSaltSync(10)
        return bcrypt.hashSync(password, salt)
    }

    const save = async (req, res) => {
        const user = { ...req.body }
        if(req.params.id) user.id_user = req.params.id
        
        //if(!req.originalUrl.startsWith('/user')) user.admin = false
        //if(!req.user || !req.user.admin) user.admin = false
        
        try {
            
            //existsOrError(user.associado_id_associado, 'Associado não  especificado')
            existsOrError(user.nome, 'Nome não informado')
            
            existsOrError(user.login, 'Login não informado')
            existsOrError(user.senha, 'Senha não informada')
            existsOrError(user.confirmPassword, 'Confirmação de Senha inválida')
            equalsOrError(user.senha, user.confirmPassword,
                'Senhas não conferem')
                
            
            const userFromDB = await app.db('user')
                .where({ login: user.login }).first()
                
            if(!user.id_user) {
                notExistsOrError(userFromDB, 'Usuário já cadastrado')
            }
        } catch(msg) {
            return res.status(400).send(msg)
        }
        
        user.senha = encryptPassword(user.senha)
        delete user.confirmPassword

        if(user.id_user) {
            app.db('user')
                .update(user)
                .where({ id_user: user.id_user })
                //.whereNull('deletedAt')
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('user')
                .insert(user)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
                console.log("teste2")
        }
    }

    const get = (req, res) => {
        app.db('user')
            .select('id_user', 'login', 'senha','nome','associado_id_associado','admin')
            //.whereNull('deletedAt')
            .then(users => res.json(users))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('user')
            .select('id_user', 'login', 'senha','nome','associado_id_associado','admin')
            .where({ id_user: req.params.id })
            //.whereNull('deletedAt')
            .first()
            .then(user => res.json(user))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        try {
            const rowsUpdated = await app.db('user')
                //.update({deletedAt: new Date()})
                .where({ id_user: req.params.id }).del()
            existsOrError(rowsUpdated, 'Usuário não foi encontrado.')

            res.status(204).send()
        } catch(msg) {
            res.status(400).send(msg)
        }
    }

    return { save, get, getById, remove }
}