module.exports = app => {
    const { existsOrError, notExistsOrError, equalsOrError } = app.api.validation

    const save = async (req, res) => {
        const associacao = { 
            tipo_associacao:req.body.tipo_associacao, 
            status_associacao:req.body.status_associacao
         }
        if(req.params.id) associacao.id_associacao = req.params.id
        /*Inserção não esta funcionado via Postman*/
        try {
            //existsOrError(associacao.tipo_associacao, 'Tipo de associação não informado')
            
            /*const userFromDB = await app.db('associacao')
                .where({ id_associacao: associacao.id_associacao }).first()
            if(!associacao.id_associacao) {
                notExistsOrError(userFromDB, 'Usuário já cadastrado')
            }*/
        } catch(msg) {
            return res.status(400).send(msg)
        }

        console.log("Teste")
        if(associacao.id_associacao) {
            app.db('associacao')
                .update(associacao)
                .where({ id_associacao: associacao.id_associacao })
                //.whereNull('deletedAt')
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            
            app.db('associacao')
                .insert(associacao)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
                console.log(associacao)
        }
    }

    const get = (req, res) => {
        app.db('associacao')
            .select('id_associacao', 'tipo_associacao', 'status_associacao')
            //.whereNull('deletedAt')
            .then(associacao => res.json(associacao))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('associado')
            .select('id_associacao', 'tipo_associacao', 'status_associacao')
            .where({ id_associacao: req.params.id })
            //.whereNull('deletedAt')
            .first()
            .then(associacao => res.json(associacao))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        try {
            const associado = await app.db('associado')
                .where({ associacao_id_associacao: req.params.id })
            notExistsOrError(associado, 'Usuário possui associacao vigente.')

            const rowsUpdated = await app.db('associacao')
                //.update({deletedAt: new Date()})
                .where({ id_associacao: req.params.id }).del()
            existsOrError(rowsUpdated, 'Associação não foi encontrado.')

            res.status(204).send()
        } catch(msg) {
            res.status(400).send(msg)
        }
    }

    return { save, get, getById, remove }
}