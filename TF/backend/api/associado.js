module.exports = app => {
    const { existsOrError, notExistsOrError, equalsOrError } = app.api.validation

    const save = async (req, res) => {
        const associado = { ...req.body }
        if (req.params.id) associado.id_associado = req.params.id

        try {
            /*existsOrError(associado.associacao_id_associacao, 'Associacao não informada')
            existsOrError(associado.telefone, 'Telefone não informado')
            existsOrError(associado.curso, 'Curso não informado')
            existsOrError(associado.cpf, 'CPF não informado')
            existsOrError(associado.rg, 'RG não informado')
            existsOrError(associado.universidade, 'Universidade não informado')
            existsOrError(associado.data_nascimento, 'Data de nascimento não informado')
            existsOrError(associado.orgao_expedidor, 'Orgao expedidor não informado')
            existsOrError(associado.endereco, 'Endereço não informado')
            existsOrError(associado.complemento, 'Nome não informado')
            existsOrError(associado.bairro, 'Bairro não informado')
            existsOrError(associado.munic, 'Municipio não informado')
            existsOrError(associado.uf, 'UF não informado')
            existsOrError(associado.cep, 'CEP não informado')
*/
           
            const userFromDB = await app.db('associado')
                .where({ rg: associado.rg }).first()
            if (!associado.id_associado) {
                notExistsOrError(userFromDB, 'Usuário já cadastrado')
            }
        } catch (msg) {
            return res.status(400).send(msg)
        }

        delete associado.tipo_associacao
        
        if (associado.id_associado) {
            app.db('associado')
                .update(associado)
                .where({ id_associado: associado.id_associado })
                //.whereNull('deletedAt')
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            
            app.db('associado')
                .insert(associado)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
              console.log("Insert ok")
              /*console.log(associado)*/
        }
    }

    const get = (req, res) => {
        app.db('associado')
            .select('id_associado', 'telefone', 'curso', 'cpf', 'rg', 'universidade', 'data_nascimento', 'orgao_expedidor',
                'endereco', 'complemento', 'bairro', 'munic', 'uf', 'cep','associacao_id_associacao')
            //.whereNull('deletedAt')
            .then(associado => res.json(associado))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('associado')
            .select('id_associado', 'telefone', 'curso', 'cpf', 'rg', 'universidade', 'data_nascimento', 'orgao_expedidor',
                'endereco', 'complemento', 'bairro', 'munic', 'uf', 'cep','associacao_id_associacao')
            .where({ id_associado: req.params.id })
            //.whereNull('deletedAt')
            .first()
            .then(associado => res.json(associado))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        try {
            const user = await app.db('user')
                .where({ associacao_id_associacao: req.params.id })
            notExistsOrError(user, 'Usuário ainda está associado.')

            const rowsUpdated = await app.db('associado')
                //.update({deletedAt: new Date()})
                .where({ id_associado: req.params.id }).del()
            existsOrError(rowsUpdated, 'Usuário não foi encontrado.')

            res.status(204).send()
        } catch (msg) {
            res.status(400).send(msg)
        }
    }

    return { save, get, getById, remove }
}