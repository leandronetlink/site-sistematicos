module.exports = app => {
    /*Importando as funções de validação do script 'validation'*/ 
    const { existsOrError, notExistsOrError, equalsOrError } = app.api.validation

    const save = async (req, res) => {
        /*Acessando todos os parametros da requisição e atribuindo no objeto 'evento'*/
        const UserEvento = { ...req.body }
        if(req.params.id) 
        UserEvento.id_user_has_eventos = req.params.id

        try {
            existsOrError(UserEvento.user_id_user, 'Admin não especificado')
            existsOrError(UserEvento.eventos_id_eventos, ' Evento não informado')
            const userFromDB = await app.db('user_has_eventos')
                .where({ id_user_has_eventos: UserEvento.id_user_has_eventos }).first()
            if(!UserEvento.id_user_has_eventos) {
                notExistsOrError(userFromDB, 'Evento já cadastrado')
            }
        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(UserEvento.id_user_has_eventos) {
            app.db('user_has_eventos')
                .update(UserEvento)
                .where({ id_user_has_eventos: UserEvento.id_user_has_eventos })
                //.whereNull('deletedAt')
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('user_has_eventos')
                .insert(UserEvento)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        }
        
    }

    const get =  (req, res) => {
        app.db('user_has_eventos')
            .select('id_user_has_eventos', 'user_id_user', 'eventos_id_eventos')
            //.whereNull('deletedAt')
            .then(evento => res.json(evento))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('user_has_eventos')
            .select('id_user_has_eventos', 'user_id_user', 'eventos_id_eventos')
            .where({ id_user_has_eventos: req.params.id })
            //.whereNull('deletedAt')
            .first()
            .then(evento => res.json(evento))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        try {
            const rowsUpdated = await app.db('user_has_eventos')
                .where({ id_user_has_eventos: req.params.id }).del()
            existsOrError(rowsUpdated, 'Relacionamento não foi encontrado.')

            res.status(204).send()
        } catch(msg) {
            res.status(400).send(msg)
        }
    }

    return { save, get, getById, remove }
}