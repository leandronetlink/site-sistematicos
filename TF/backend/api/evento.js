module.exports = app => {
    /*Importando as funções de validação do script 'validation'*/ 
    const { existsOrError, notExistsOrError, equalsOrError } = app.api.validation

    const save = async (req, res) => {
        /*Acessando todos os parametros da requisição e atribuindo no objeto 'evento'*/
        const evento = { ...req.body }
        if(req.params.id) 
        evento.id_eventos = req.params.id

        try {
            existsOrError(evento.titulo_evento, 'Titulo do evento nao informado')
            existsOrError(evento.descricao, 'Descricao do evento não informado')
            const userFromDB = await app.db('eventos')
                .where({ titulo_evento: evento.titulo_evento }).first()
            if(!evento.id_eventos) {
                notExistsOrError(userFromDB, 'Evento já cadastrado')
            }
        } catch(msg) {
            return res.status(400).send(msg)
        }

        if(evento.id_eventos) {
            app.db('eventos')
                .update(evento)
                .where({ id_eventos: evento.id_eventos })
                //.whereNull('deletedAt')
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        } else {
            app.db('eventos')
                .insert(evento)
                .then(_ => res.status(204).send())
                .catch(err => res.status(500).send(err))
        }
        
    }

    const get =  (req, res) => {
        app.db('eventos')
            .select('id_eventos', 'titulo_evento', 'descricao','data_evento')
            //.whereNull('deletedAt')
            .then(evento => res.json(evento))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        app.db('eventos')
            .select('id_eventos', 'titulo_evento', 'descricao','data_evento')
            .where({ id_eventos: req.params.id })
            //.whereNull('deletedAt')
            .first()
            .then(evento => res.json(evento))
            .catch(err => res.status(500).send(err))
    }

    const remove = async (req, res) => {
        try {
            const rowsUpdated = await app.db('eventos')
                .where({ id_eventos: req.params.id }).del()
            existsOrError(rowsUpdated, 'Evento não foi encontrado.')

            res.status(204).send()
        } catch(msg) {
            res.status(400).send(msg)
        }
    }

    return { save, get, getById, remove }
}