const app = require('express')()
const consign = require('consign')
/*Integrando arquivo do bd*/ 
const db = require('./config/db')
/*Atribuindo o db criado no backend do sistema*/ 
app.db= db


consign()
    .include('./config/passport.js')
    .then('./config/middlewares.js')
    .then('./api/validation.js')
    .then('./api')
    .then('./config/routes.js')
    .into(app)



app.listen(4000, ()=>{
    console.log('Backend executando....')
})