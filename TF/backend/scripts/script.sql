-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Administrador` (
  `idAdministrador` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `user` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAdministrador`),
  UNIQUE INDEX `idAdministrador_UNIQUE` (`idAdministrador` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Eventos` (
  `idEventos` INT NOT NULL AUTO_INCREMENT,
  `titulo_evento` VARCHAR(45) NOT NULL,
  `data_evento` DATE NULL,
  `descricao` LONGTEXT NOT NULL,
  PRIMARY KEY (`idEventos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Associacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Associacao` (
  `idAssociacao` INT NOT NULL AUTO_INCREMENT,
  `tipo_associacao` VARCHAR(45) NOT NULL,
  `status_associacao` TINYINT NULL,
  PRIMARY KEY (`idAssociacao`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Associado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Associado` (
  `idAssociado` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `telefone` VARCHAR(45) NOT NULL,
  `curso` VARCHAR(45) NOT NULL,
  `cpf` VARCHAR(45) NOT NULL,
  `rg` VARCHAR(45) NOT NULL,
  `universidade` VARCHAR(45) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `rg_orgao_expedidor` VARCHAR(10) NOT NULL,
  `endereco` VARCHAR(100) NOT NULL,
  `complemento` VARCHAR(45) NOT NULL,
  `bairro` VARCHAR(45) NOT NULL,
  `municipio` VARCHAR(45) NOT NULL,
  `uf` CHAR(2) NOT NULL,
  `cep` DECIMAL(8) NOT NULL,
  `Associacao_idAssociacao` INT NOT NULL,
  `login` VARCHAR(30) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAssociado`),
  INDEX `fk_Associado_Associacao_idx` (`Associacao_idAssociacao` ASC),
  CONSTRAINT `fk_Associado_Associacao`
    FOREIGN KEY (`Associacao_idAssociacao`)
    REFERENCES `mydb`.`Associacao` (`idAssociacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Administrador_has_Associacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Administrador_has_Associacao` (
  `Administrador_idAdministrador` INT NOT NULL,
  `Associacao_idAssociacao` INT NOT NULL,
  PRIMARY KEY (`Administrador_idAdministrador`, `Associacao_idAssociacao`),
  INDEX `fk_Administrador_has_Associacao_Associacao1_idx` (`Associacao_idAssociacao` ASC),
  INDEX `fk_Administrador_has_Associacao_Administrador1_idx` (`Administrador_idAdministrador` ASC),
  CONSTRAINT `fk_Administrador_has_Associacao_Administrador1`
    FOREIGN KEY (`Administrador_idAdministrador`)
    REFERENCES `mydb`.`Administrador` (`idAdministrador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Administrador_has_Associacao_Associacao1`
    FOREIGN KEY (`Associacao_idAssociacao`)
    REFERENCES `mydb`.`Associacao` (`idAssociacao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Administrador_has_Eventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Administrador_has_Eventos` (
  `Administrador_idAdministrador` INT NOT NULL,
  `Eventos_idEventos` INT NOT NULL,
  PRIMARY KEY (`Administrador_idAdministrador`, `Eventos_idEventos`),
  INDEX `fk_Administrador_has_Eventos_Eventos1_idx` (`Eventos_idEventos` ASC),
  INDEX `fk_Administrador_has_Eventos_Administrador1_idx` (`Administrador_idAdministrador` ASC),
  CONSTRAINT `fk_Administrador_has_Eventos_Administrador1`
    FOREIGN KEY (`Administrador_idAdministrador`)
    REFERENCES `mydb`.`Administrador` (`idAdministrador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Administrador_has_Eventos_Eventos1`
    FOREIGN KEY (`Eventos_idEventos`)
    REFERENCES `mydb`.`Eventos` (`idEventos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `Associado_idAssociado` INT NOT NULL,
  `Administrador_idAdministrador` INT NOT NULL,
  PRIMARY KEY (`idUser`),
  INDEX `fk_User_Associado1_idx` (`Associado_idAssociado` ASC),
  INDEX `fk_User_Administrador1_idx` (`Administrador_idAdministrador` ASC),
  CONSTRAINT `fk_User_Associado1`
    FOREIGN KEY (`Associado_idAssociado`)
    REFERENCES `mydb`.`Associado` (`idAssociado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_Administrador1`
    FOREIGN KEY (`Administrador_idAdministrador`)
    REFERENCES `mydb`.`Administrador` (`idAdministrador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
