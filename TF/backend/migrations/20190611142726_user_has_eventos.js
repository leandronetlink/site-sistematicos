
exports.up = function(knex, Promise) {
    return knex.schema.createTable('user_has_eventos', function (table) {
        table.increments('id_user_has_eventos').primary().unique().notNullable()
        table.integer('user_id_user');
        table.integer('eventos_id_eventos');
        table.foreign('eventos_id_eventos').references('eventos.id_eventos');
        table.foreign('user_id_user').references('user.id_user');
      })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user_has_eventos');
};
