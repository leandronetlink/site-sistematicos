
exports.up = function(knex, Promise) {
    return knex.schema.createTable('associado', function (table) {
        table.increments('id_associado').primary().unique().notNullable();
        table.string('telefone');
        table.string('curso');
        table.string('cpf');
        table.string('rg');
        table.string('universidade');
        table.date('data_nascimento')
        table.string('orgao_expedidor',10);
        table.string('endereco');
        table.string('complemento');
        table.string('bairro');
        table.string('munic');
        table.string('uf',2);
        table.string('cep',8);
        table.integer('associacao_id_associacao');
        table.foreign('associacao_id_associacao').references('associacao.id_associacao')
      })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('associado');
};
