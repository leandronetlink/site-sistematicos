
exports.up = function(knex, Promise) {
    return knex.schema.createTable('associacao', function (table) {
        table.increments('id_associacao').primary().unique().notNullable();
        table.string('tipo_associacao');
        table.integer('status_associacao');
      })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('associacao');
};
