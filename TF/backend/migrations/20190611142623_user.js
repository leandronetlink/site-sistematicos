
exports.up = function(knex, Promise) {
    return knex.schema.createTable('user', function (table) {
        table.increments('id_user').primary().unique().notNullable();
        table.string('nome');
        table.string('login');
        table.string('senha');
        table.boolean('admin').notNull().defaultTo(false); /*Tipo boolean*/
        table.integer('associado_id_associado');
        table.foreign('associado_id_associado').references('associado.id_associado');
      })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user');
};
