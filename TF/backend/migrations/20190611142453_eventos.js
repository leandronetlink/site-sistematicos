
exports.up = function(knex, Promise) {
    return knex.schema.createTable('eventos', function (table) {
        table.increments('id_eventos').primary().unique().notNullable();
        table.string('titulo_evento').notNull();
        table.string('descricao').notNull();
        table.date('data_evento');
      })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('eventos');
};
