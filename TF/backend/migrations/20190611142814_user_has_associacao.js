
exports.up = function(knex, Promise) {
    return knex.schema.createTable('user_has_associacao', function (table) {
        table.increments('id_user_has_associacao').primary().unique().notNullable()
        table.integer('user_id_user');
        table.integer('associacao_id_associacao');
        table.foreign('associacao_id_associacao').references('associacao.id_associacao');
        table.foreign('user_id_user').references('user.id_user');
      })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user_has_associacao');
};
